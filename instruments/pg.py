# -*- coding: utf-8 -*-
from . import rm
import time

ChA = 1
ChB = 2
ChC = 3
ChD = 4
ChE = 5
ChF = 6
ChG = 7
ChH = 8

channels = list(range(1,9))

class pg:
    def __init__(self, visa_name):
        #this baseline will start 1 s after the T0
        self.baseline_conf = {ChA : 0.0102, ChE : 0.002,
                              ChB : 0.9950, ChH : 0.912}
                              
        self.conf_stack = []
        
        self.visa_handle = rm.open_resource(visa_name)
        print("Succesfully opened DTG "+
                self.visa_handle.query('*IDN?'))
    def __enter__(self):
        return self
    def close(self):
        self.visa_handle.close()
    def __exit__(self):
        self.close()
    
    def run(self):
        return self.visa_handle.query(':SPULSE:STAT 1')
    def stop(self):
        return self.visa_handle.query(':SPULSE:STAT 0')
    def read_conf(self):
        conf = {}
        for ch in channels:
            query = ':PULSE{}:DELAY?'.format(ch)
            delay = self.visa_handle.query(query)
            conf[ch] = float(delay)
        return conf
    
    def set_delay(self, channel, delay):
        return self.visa_handle.query(":PULSE{}:DELAY {:.5}".format(
                                       channel, delay))
    def set_timing(self, drift, decay):
        assert(decay > 0.088)
        new_conf = self.baseline_conf.copy()
        new_conf[ChA] += drift
        new_conf[ChE] += drift
        new_conf[ChH] += drift
        
        new_conf[ChB] += decay - 1
        new_conf[ChH] += decay - 1
        
        self.load_conf(new_conf)
    
    def save_conf(self):
        conf = self.read_conf()        
        self.conf_stack.append(conf)
        return conf
    
    def load_conf(self, conf = None):
        if conf is None:
            conf = self.conf_stack.pop()
        for ch in conf:
            self.set_delay(ch, conf[ch])