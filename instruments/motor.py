# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 13:44:30 2016

@author: emil
"""

from . import rm
from visa import constants as c

class motor:
    def __init__(self, visa_name):
        self.visa_handle = rm.open_resource(visa_name)
        self.visa_handle.baud_rate = 9600
        self.visa_handle.data_bits = 8
        self.visa_handle.stop_bits = c.StopBits.one
        self.visa_handle.parity    = c.Parity.none
        self.visa_handle.flow_control = c.VI_ASRL_FLOW_XON_XOFF
        #5 s timeout on communication
        self.visa_handle.timeout = 5000
        
        self.max_D = 15000
        self.D = 10000
        
    def __enter__(self):
        return self
        
    def __exit__(self):
        self.visa_handle.close()
        
    def get_pos(self):
        self.visa_handle.write("TPM")
        tpm = self.visa_handle.read() #echo
        tpm = self.visa_handle.read() #the response
        #the response has the form "*TPM<number>\r\r\n"
        return int(tpm[4:])
    
    def set_velocity_rps(self, v):
        self.visa_handle.write('V {:.3f}'.format(v))
    
    def set_velocity_mms(self, v):
        self.visa_handle.write('V {:.3f}'.format(v/250.0))

    def set_accel_rps2(self, a):
        self.visa_handle.write('A {:.3f}'.format(a))
    
    def up(self):
        pos = self.get_pos()
        if pos > 0:
            raise RuntimeError("motor already up")
        self.visa_handle.write('UP')
    
    def down(self):
        pos = self.get_pos()
        if pos < 0:
            raise RuntimeError("motor already down")
        self.visa_handle.w2rite('DOWN')