# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 13:51:26 2016

@author: emil

this module calls a VB script that tells the WinView program to start
measurement
"""

import subprocess as sub
import os.path as path

def init_image_capture(dirpath, filename):
    """
        starts an image capture in the WinView32 program which must
        be running; executino is blocked until the image is written
        to the disk
        
        dirpath = directory name; should have the trailing '\\'
        filename = file name of the image
        
        the "camera_control_macro.VBS" must be in the same
        directory as this file
    """
    sub.call(["wscript.exe", 
              path.join(path.dirname(__file__),
                        "camera_control_macro.VBS"),
              dirpath, filename])
    return path.join(dirpath, filename)


if __name__ == '__main__':
    init_image_capture('C:\\tests\\', 'testfile')