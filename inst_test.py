# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 16:41:11 2016

@author: Tony
"""

import instruments

if __name__ == '__main__':
    dtg = instruments.pg.pg('ASRL3::INSTR')
    print(dtg.read_conf())
    dtg.close()