# -*- coding: utf-8 -*-
"""
Created on Wed Sep 14 10:41:44 2016

@author: emil
"""

import yaml
from copy import copy

class ExpJob:
    def __init__(self, **entries):
        self.__dict__.update(entries)
    def __str__(self):
        return str(self.__dict__)

def load_jobs(jobs_file):
    jobs = []
    with open(jobs_file) as f:
        data = yaml.safe_load(f)
        for key in data:
            #make sure the entry defines all the needed parameters
            params = data[key].keys()
            needed_params = set(['velocity', 'drift',
                                 'decay', 'reps'])
            if needed_params.issubset(params):
                new_job = ExpJob(**data[key])
                if 'order' not in params:
                    new_job.order = 99
                jobs.append(copy(new_job))
            else:#if not, complain
                raise ValueError(
                    '{} lacks necessary parameters {}(perhaps a typo?)'.
                        format(key, needed_params-params))
    jobs.sort(key=lambda j: j.order)
    return jobs

if __name__ == '__main__':
    jobs = load_jobs('jobs.yaml')
    for job in jobs:
        print(job)