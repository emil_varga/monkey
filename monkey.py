# -*- coding: utf-8 -*-
"""
Created on Wed Sep 14 09:57:23 2016

@author: emil
"""

import instruments.camera as cam
from instruments.motor import motor
from instruments.pg import pg
import ExpJobs

import time
import sys

if __name__ == '__main__':
    if(len(sys.argv) < 2):
        print('please provide a job file')
        exit(-1)
    jobs = ExpJobs.load_jobs(sys.argv[1])
    with motor('ASRLx::INSTR') as M, pg('ASRLy::INSTR') as DTG:
        for job in jobs:
            #load the timing
            DTG.set_timing(decay=job.decay, drift=job.drift)
            for k in range(job.reps):
                if k % 10 == 0: #baseline
                    DTG.stop()
                    DTG.save_conf() #saves the currnet config on stack
                    DTG.load_conf(DTG.baseline_conf)
                    DTG.run()
                    cam.init_image_capture()
                    DTG.stop()
                    DTG.load_conf() #loads the confing from the top
                                    #of the stack
                    DTG.run()
                    time.sleep(10) #wait 10 s
                else: #ordinary image
                    cam.init_image_capture()
                    time.sleep(30) #wait 30 s